﻿
using AppWCFService.Services;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class SistemaController
    {

        public  List<logo> Getlogos( int ? Clv_plaza)
        {
            List<logo> lista = new List<logo>();
            try
            {
                dbHelper db = new dbHelper();                
                db.agregarParametro("@Clv_plaza", SqlDbType.BigInt, Clv_plaza);
                SqlDataReader rd = db.consultaReader("getlistaLogo");
                while (rd.Read())
                {
                    logo logo = new logo();
                    logo.Valor = rd[0].ToString();                    
                    lista.Add(logo);

                    logo logo2 = new logo();
                    logo2.Valor = rd[1].ToString();
                    lista.Add(logo2);
                }
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch(Exception ex){}
           
           return lista;
        }

        public int ? GuardaLogo(Stream archivo, int tipo, int Clv_plaza, string URL) {

            string folder = System.AppDomain.CurrentDomain.BaseDirectory + "logos/";
            string imagen = Guid.NewGuid().ToString() + ".png";
            System.Drawing.Image img = System.Drawing.Image.FromStream(archivo);
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            img.Save(folder + "\\" + imagen, ImageFormat.Png);

            dbHelper db = new dbHelper();
            db.agregarParametro("@idtipo", SqlDbType.BigInt, tipo);
            db.agregarParametro("@valor", SqlDbType.VarChar, imagen);           
            db.agregarParametro("@Clv_plaza", SqlDbType.Int, Clv_plaza);
            db.agregarParametro("@URL", SqlDbType.VarChar, URL);
            db.consultaSinRetorno("ActualizaLogo");
            db.conexion.Close();
            db.conexion.Dispose();            
            return 1;
        }

        

         




    }
}