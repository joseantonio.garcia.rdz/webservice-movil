﻿using AppWCFService.Services;
using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace AppWCFService.Controllers
{
    public class MovilController
    {
        public DatosCliente GetClienteMovil(string Usuario,string Contraseña)
        {
            DatosCliente lista = new DatosCliente();
            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@Usuario", SqlDbType.VarChar, Usuario);
                db.agregarParametro("@Contraseña", SqlDbType.VarChar, Contraseña);
                SqlDataReader rd = db.consultaReader("GetClienteMovil");
                lista = db.MapDataToEntityCollection<DatosCliente>(rd).ToList()[0];
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex) { }

            return lista;
        }

        public TokenCelular AddClienteToken(int? Contrato, string Token)
        {
            TokenCelular lista = new TokenCelular();
            
            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@Contrato", SqlDbType.BigInt, Contrato);
                db.agregarParametro("@Token", SqlDbType.VarChar, Token);
                SqlDataReader rd = db.consultaReader("AddClienteToken");
                lista = db.MapDataToEntityCollection<TokenCelular>(rd).ToList()[0];
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex) { }

            return lista;
        }


        public ClienteDatos GetClienteDatos(long Contrato)
        {
            ClienteDatos lista = new ClienteDatos();
            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@Contrato", SqlDbType.VarChar, Contrato);
                SqlDataReader rd = db.consultaReader("GetClienteDatos");
                lista = db.MapDataToEntityCollection<ClienteDatos>(rd).ToList()[0];
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex) { }

            return lista;
        }

        public List<ServiciosCliente> GetClienteServicios(long Contrato)
        {
            List<ServiciosCliente> lista = new List<ServiciosCliente>();
            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@CONTRATO", SqlDbType.VarChar, Contrato);
                SqlDataReader rd = db.consultaReader("DAMEServiciosDelClienteAndroid");
                while (rd.Read())
                {
                    ServiciosCliente lista1 = new ServiciosCliente();
                    lista1.TipServ = Int32.Parse(rd[0].ToString());
                    lista1.Servicio = rd[1].ToString();
                    lista1.Status = rd[2].ToString();
                    lista.Add(lista1);
                }
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex) { }

            return lista;
        }

        public List<DetalleCobroCliente> GetClienteCobroCliente(long Contrato)
        {
            List<DetalleCobroCliente> lista = new List<DetalleCobroCliente>();
            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@CONTRATO", SqlDbType.VarChar, Contrato);
                SqlDataReader rd = db.consultaReader("ObtieneDetalleCobro");
                while (rd.Read())
                {
                    DetalleCobroCliente lista1 = new DetalleCobroCliente();
                    lista1.description = rd[0].ToString();
                    lista1.amount = decimal.Parse(rd[1].ToString());
                    lista1.operation = rd[2].ToString();
                    lista1.FechaConsulta = rd[3].ToString();
                    lista.Add(lista1);
                }
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex) { }

            return lista;
        }

        public Ejemplo GetEjemplo(string Status, int ClvTipSer)
        {
            Ejemplo lista = new Ejemplo();
            List<EjemploLista0> EjemploLista0 = new List<EjemploLista0>();
            List<EjemploLista1> EjemploLista1 = new List<EjemploLista1>();
            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@Satuts", SqlDbType.VarChar, Status);
                db.agregarParametro("@ClvTipSer", SqlDbType.Int, ClvTipSer);
                SqlDataReader rd = db.consultaReader("EjemploLista");
                while (rd.Read())
                {
                    EjemploLista0 ejemplo = new EjemploLista0();
                    ejemplo.ClvOrd0 = Int32.Parse(rd[0].ToString());
                    ejemplo.Contrato0 = Int32.Parse(rd[1].ToString());
                    EjemploLista0.Add(ejemplo);
                }
                if (rd.NextResult())
                {
                    while (rd.Read())
                    {
                        EjemploLista1 ejemplo1 = new EjemploLista1();
                        ejemplo1.ClvOrd1 = Int32.Parse(rd[0].ToString());
                        ejemplo1.Contrato1 = Int32.Parse(rd[1].ToString());
                        EjemploLista1.Add(ejemplo1);
                    }
                }
                lista.lista0 = EjemploLista0;
                lista.lista1 = EjemploLista1;
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex) { }

            return lista;
        }



    }
}