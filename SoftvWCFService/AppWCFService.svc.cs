﻿

using AppWCFService.Contracts;
using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel.Web;
using System.Web.Script.Services;
using AppWCFService.Controllers;
using Softv.Entities;
using System.Web;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Drawing;
using System.Text;
using System.Xml.Linq;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Newtonsoft.Json;

namespace AppWCFService
{
    [ScriptService]
    public partial class AppWCFService : ISistema, IMovil
    {
        SistemaController SistemaController = new SistemaController();
        MovilController PruebasController = new MovilController();
        #region Almacen

        public List<logo> Getlogos()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                var request = HttpContext.Current.Request;
                int Clv_plaza = Int32.Parse(request["Clv_plaza"].ToString());
                return SistemaController.Getlogos(Clv_plaza);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public int ? Guardalogos()
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            else
            {
                try
                {
                    var request = HttpContext.Current.Request;
                    int tipo =Int32.Parse(request["tipo"].ToString());                   
                    int Clv_plaza = Int32.Parse(request["Clv_plaza"].ToString());
                    string URL = request["URL"].ToString();
                    if (request.Files.Count > 0)
                    {                       
                        Stream streamed = request.Files[0].InputStream;
                        return SistemaController.GuardaLogo(streamed,tipo,Clv_plaza, URL);
                    }else
                    {
                        throw new WebFaultException<string>("Archivo no encontrado", HttpStatusCode.ExpectationFailed);
                    }
                   
                }
                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }
            }

        }


        #endregion

        #region LogOn


        private const string Secret = "db3OIsj+BXE9NZDy0t8W3TcNekrF+2d/1sFnWG4HnV8TZY30iTOdtVWJG8abWvB1GlOgJuQZdcF2Luqm/hccMw==";
        private const string BasicAuth = "Basic";

        public static string GenerateToken(string username, int expireMinutes = 20000)
        {
            var symmetricKey = Convert.FromBase64String(Secret);
            var tokenHandler = new JwtSecurityTokenHandler();

            var now = DateTime.UtcNow;
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                        {
                        new Claim(ClaimTypes.Name, username)
                    }),

                Expires = now.AddMinutes(Convert.ToInt32(expireMinutes)),

                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(symmetricKey), SecurityAlgorithms.HmacSha256Signature)
            };

            var stoken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(stoken);

            return token;
        }
         public LoginCliente LogOn()
        {
            LoginCliente objsession = new LoginCliente();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            if (WebOperationContext.Current.IncomingRequest.Headers["Authorization"] == null)
            {
                //WebOperationContext.Current.OutgoingResponse.Headers.Add("WWW-Authenticate: Basic realm=\"myrealm\"");
                throw new WebFaultException<string>("Acceso no autorizado, favor de validar autenticación", HttpStatusCode.Unauthorized);
            }
            else // Decode the header, check password
            {
                string encodedUnamePwd = GetEncodedCredentialsFromHeader();
                if (!string.IsNullOrEmpty(encodedUnamePwd))
                {
                    byte[] decodedBytes = null;
                    try
                    {
                        decodedBytes = Convert.FromBase64String(encodedUnamePwd);
                    }
                    catch (FormatException)
                    { }

                    string credentials = ASCIIEncoding.ASCII.GetString(decodedBytes);
                    string[] authParts = credentials.Split(':');
                    DatosCliente objUsr = GetClienteMovil(authParts[0], authParts[1]);
                    if (objUsr != null)
                    {
                        //objsession = SessionWeb.GetAuthentication(authParts[0], authParts[1]);
                        objsession.token = GenerateToken(authParts[0]);
                        objsession.Usuario = objUsr.Usuario;
                        objsession.Contrato = objUsr.Contrato;
                        return objsession;
                    }
                    else
                    {
                        // WebOperationContext.Current.OutgoingResponse.Headers.Add("WWW-Authenticate: Basic realm=\"myrealm\"");
                        throw new WebFaultException<string>("Acceso no autorizado, favor de validar autenticación", HttpStatusCode.Unauthorized);
                    }
                }
            }

            return objsession;
        }


         private static string GetEncodedCredentialsFromHeader()
        {
            WebOperationContext ctx = WebOperationContext.Current;

            // credentials are in the Authorization Header
            string credsHeader = ctx.IncomingRequest.Headers[HttpRequestHeader.Authorization];
            if (credsHeader != null)
            {
                // make sure that we have 'Basic' auth header. Anything else can't be handled
                string creds = null;
                int credsPosition = credsHeader.IndexOf(BasicAuth, StringComparison.OrdinalIgnoreCase);
                if (credsPosition != -1)
                {
                    // 'Basic' creds were found
                    credsPosition += BasicAuth.Length + 1;
                    if (credsPosition < credsHeader.Length - 1)
                    {
                        creds = credsHeader.Substring(credsPosition, credsHeader.Length - credsPosition);
                        return creds;
                    }
                    return null;
                }
                else
                {
                    // we did not find Basic auth header but some other type of auth. We can't handle it. Return null.
                    return null;
                }
            }

            // no auth header was found
            return null;
        }


#endregion

        #region Movil
        public DatosCliente GetClienteMovil(string Usuario, string Contraseña)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {
                
                return PruebasController.GetClienteMovil(Usuario,Contraseña);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public TokenCelular AddClienteToken(int? Contrato, string Token)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return PruebasController.AddClienteToken(Contrato, Token);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        public ClienteDatos GetClienteDatos(long Contrato)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return PruebasController.GetClienteDatos(Contrato);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public List<ServiciosCliente> GetClienteServicios(long Contrato)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return PruebasController.GetClienteServicios(Contrato);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        public List<DetalleCobroCliente> GetClienteCobroCliente(long Contrato)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return PruebasController.GetClienteCobroCliente(Contrato);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }






        public Ejemplo GetEjemplo(string Status, int ClvTipSer)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return PruebasController.GetEjemplo(Status, ClvTipSer);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


        #endregion
    }
}
