﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    public class DatosCliente
    {
        public long Contrato { get; set; }

        public string Usuario { get; set; }


    }
    public class LoginCliente
    {
        public String token { get; set; }

        public String Usuario { get; set; }

        public long Contrato { get; set; }


    }
    public class TokenCelular
    {
        public long Añadido { get; set; }

    }


    public class ClienteDatos
    {
        public long Contrato { get; set; }

        public String Nombre { get; set; }

        public long Telefono { get; set; }

        public String Calle { get; set; }

        public long Numero { get; set; }

        public String Colonia { get; set; }


    }

    public class ServiciosCliente
    {
        public long TipServ { get; set; }
        public String Servicio { get; set; }

        public String Status { get; set; }


    }


    public class DetalleCobroCliente
    {
        public String description { get; set; }
        public decimal amount { get; set; }

        public String operation { get; set; }
        public String FechaConsulta { get; set; }

    }


    public class Ejemplo
    {
        public List<EjemploLista0> lista0 { get; set; }

        public List<EjemploLista1> lista1 { get; set; }

    }
    public class EjemploLista0
    {
        public long ClvOrd0 { get; set; }
        public long Contrato0 { get; set; }

    }
    public class EjemploLista1
    {
        public long ClvOrd1 { get; set; }
        public long Contrato1 { get; set; }

    }

}