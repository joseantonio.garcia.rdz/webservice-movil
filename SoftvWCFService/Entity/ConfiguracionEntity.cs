﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Softv.Entities
{
    


    [DataContract]
    [Serializable]
    public class PreferenciasEntity 
    {
        #region Attributes

        
        [DataMember]
        public string NombreSistema { get; set; }
        [DataMember]
        public string TituloNav { get; set; }
        [DataMember]
        public string ColorMenu { get; set; }

        [DataMember]
        public string ColorMenuLetra { get; set; }

        [DataMember]
        public string ColorNav { get; set; }

        [DataMember]
        public string ColorNavLetra { get; set; }

        [DataMember]
        public string MensajeHome { get; set; }

        [DataMember]
        public string ColorFondo { get; set; }


        #endregion
    }


    [DataContract]
    [Serializable]
    public class GeneralesEmpresa 
    {
        [DataMember]
        public int? Clv_plaza { get; set; }
        [DataMember]
        public string LogoAplicacion { get; set; }
        [DataMember]
        public string LogoReportes { get; set; }

    }
}





















