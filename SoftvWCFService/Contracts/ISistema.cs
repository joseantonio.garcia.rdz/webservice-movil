﻿using Softv.Entities;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISistema
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Getlogos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<logo> Getlogos();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Guardalogos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        int? Guardalogos();






    }
}