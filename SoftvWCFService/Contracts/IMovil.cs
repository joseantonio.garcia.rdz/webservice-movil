﻿using Softv.Entities;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace AppWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IMovil
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClienteMovil", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DatosCliente GetClienteMovil(string Usuario, string Contraseña);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddClienteToken", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        TokenCelular AddClienteToken(int? Contrato, string Token);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEjemplo", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Ejemplo GetEjemplo(string Status, int ClvTipSer);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "LogOn", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        LoginCliente LogOn();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClienteDatos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ClienteDatos GetClienteDatos(long Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClienteServicios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ServiciosCliente> GetClienteServicios(long Contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClienteCobroCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DetalleCobroCliente> GetClienteCobroCliente(long Contrato);
    }
}